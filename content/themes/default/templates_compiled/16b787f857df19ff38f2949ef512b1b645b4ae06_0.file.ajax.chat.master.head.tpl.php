<?php
/* Smarty version 3.1.32, created on 2019-12-07 16:36:10
  from 'C:\xampp\htdocs\redsocial\content\themes\default\templates\ajax.chat.master.head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5debd4fab6e954_83790986',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '16b787f857df19ff38f2949ef512b1b645b4ae06' => 
    array (
      0 => 'C:\\xampp\\htdocs\\redsocial\\content\\themes\\default\\templates\\ajax.chat.master.head.tpl',
      1 => 1575734991,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5debd4fab6e954_83790986 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['offline']->value) {?>

    <div class="chat-head-title">
        <i class="fa fa-user-secret"></i>
        <?php echo __("Offline");?>

    </div>

<?php } else { ?>

    <div class="chat-head-title">
        <i class="fa fa-circle"></i>
        (<?php echo count($_smarty_tpl->tpl_vars['online_friends']->value);?>
)
    </div>

<?php }
}
}
